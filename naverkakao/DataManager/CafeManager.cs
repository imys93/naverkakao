﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace naverkakao
{
    public class CafeManager : List<Model_cafe>
    {
        const string datapath = "./data/cafedata.json";

        public CafeManager()
        {
            this.loadData();
        }

        private void loadData()
        {
            this.Clear();
            try
            {
                var jsonstring = System.IO.File.ReadAllText(datapath);

                List<Model_cafe> obj = JsonConvert.DeserializeObject<List<Model_cafe>>(jsonstring);
                if(obj != null)
                {
                    this.AddRange(obj);

                }
            }
            catch (System.IO.FileNotFoundException)
            {
                this.saveData();

            }

            //this.AddRange();
            //= JsonConvert.DeserializeObject()
        }

        private void saveData()
        {
            System.IO.File.WriteAllText(datapath, JsonConvert.SerializeObject(this));
        }

        public bool addCafe(string cafe_id, string templateString)
        {
            Model_cafe mc = new Model_cafe();
            mc.init(0, cafe_id, templateString);
            this.Add(mc);
            this.saveData();
            return true;
        }

        public bool removeCafe(int index)
        {
            if(index >= 0)
            {

                this.RemoveAt(index);
                this.saveData();
                return true;
            }
            return false;
        }

        public void setTemplate(int index, string templateString)
        {
            this[index].setTemplate(templateString);
            this.saveData();
        }

        public bool modifyCafe()
        {
            return true;
        }

        public bool deleteCafe()
        {
            return true;
        }

        public void refreshCafes()
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i].updateInfo();
            }
            this.saveData();
        }
    }
}



