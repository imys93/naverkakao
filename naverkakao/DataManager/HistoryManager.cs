﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace naverkakao
{
    public class HistoryManager : List<Model_history>
    {
        const string datapath = "./data/historydata.json";

        public HistoryManager()
        {
            this.loadData();
        }

        private void loadData()
        {
            this.Clear();
            try
            {
                var jsonstring = System.IO.File.ReadAllText(datapath);

                List<Model_history> obj = JsonConvert.DeserializeObject<List<Model_history>>(jsonstring);
                if(obj != null)
                {
                    this.AddRange(obj);
                }   
            }
            catch (System.IO.FileNotFoundException)
            {
                this.saveData();

            }

            //this.AddRange();
            //= JsonConvert.DeserializeObject()
        }

        public void saveLog(string midea_id, string post_id)
        {
            Model_history mh = new Model_history();
            mh.init();
            mh.midea_id = midea_id;
            mh.post_id = post_id;
            this.Add(mh);
            this.saveData();
        }


        public void saveData()
        {
            System.IO.File.WriteAllText(datapath, JsonConvert.SerializeObject(this));
        }
    }
}



