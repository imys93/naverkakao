﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace naverkakao
{
    public class KakaoManager : List<Model_kakao>
    {
        const string datapath = "./data/kakaodata.json";

        public KakaoManager()
        {
            this.loadData();
        }

        private void loadData()
        {
            this.Clear();
            try
            {
                var jsonstring = System.IO.File.ReadAllText(datapath);

                List<Model_kakao> obj = JsonConvert.DeserializeObject<List<Model_kakao>>(jsonstring);
                if(obj != null)
                {
                    this.AddRange(obj);
                }
                
            }
            catch (System.IO.FileNotFoundException)
            {
                this.saveData();
            }

            //this.AddRange();
            //= JsonConvert.DeserializeObject()
        }

        public void saveData()
        {
            System.IO.File.WriteAllText(datapath, JsonConvert.SerializeObject(this));
        }
    }
}



