﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace naverkakao
{
    public class PostManager : List<Model_post>
    {
        const string datapath = "./data/postdata.json";

        public PostManager()
        {
            this.loadData();
        }

        private void loadData()
        {
            this.Clear();
            try
            {
                var jsonstring = System.IO.File.ReadAllText(datapath);

                List<Model_post> obj = JsonConvert.DeserializeObject<List<Model_post>>(jsonstring);
                if(obj != null)
                {
                    this.AddRange(obj);
                }
                
            }
            catch (System.IO.FileNotFoundException)
            {
                this.saveData();

            }

            //this.AddRange();
            //= JsonConvert.DeserializeObject()
        }


        public void saveData()
        {
            System.IO.File.WriteAllText(datapath, JsonConvert.SerializeObject(this));
        }
    }
}



