﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace naverkakao
{
    class ShopCategoryManager : List <Model_category>
    {
        const string datapath = "./data/shopdata.json";
        public ShopCategoryManager()
        {
            this.loadData();
        }

        public void refreshData()
        {
            this.Clear();

            ShopCrawler crawler = new ShopCrawler();

            List<Model_category> cate = crawler.getCategorys();

            this.AddRange(cate);

            this.saveData();

        }
        private void loadData()
        {
            this.Clear();
            try
            {
                var jsonstring = System.IO.File.ReadAllText(datapath);

                List<Model_category> obj = JsonConvert.DeserializeObject<List<Model_category>>(jsonstring);
                if (obj != null)
                {
                    this.AddRange(obj);
                }

            }
            catch (System.IO.FileNotFoundException)
            {
                ShopCrawler crawler = new ShopCrawler();

                List<Model_category> cate = crawler.getCategorys();

                this.AddRange(cate);

                this.saveData();

            }

            //this.AddRange();
            //= JsonConvert.DeserializeObject()
        }


        public void saveData()
        {
            System.IO.File.WriteAllText(datapath, JsonConvert.SerializeObject(this));
        }
    }
}
