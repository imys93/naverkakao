﻿namespace naverkakao
{
    partial class cafeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_cafe = new System.Windows.Forms.ListBox();
            this.button_add = new System.Windows.Forms.Button();
            this.button_mod = new System.Windows.Forms.Button();
            this.button_del = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBox_cafe
            // 
            this.listBox_cafe.FormattingEnabled = true;
            this.listBox_cafe.ItemHeight = 12;
            this.listBox_cafe.Location = new System.Drawing.Point(12, 82);
            this.listBox_cafe.Name = "listBox_cafe";
            this.listBox_cafe.Size = new System.Drawing.Size(419, 100);
            this.listBox_cafe.TabIndex = 0;
            this.listBox_cafe.SelectedIndexChanged += new System.EventHandler(this.listBox_cafe_SelectedIndexChanged);
            this.listBox_cafe.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox_cafe_MouseDoubleClick);
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(194, 188);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(75, 23);
            this.button_add.TabIndex = 1;
            this.button_add.Text = "추가";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_mod
            // 
            this.button_mod.Location = new System.Drawing.Point(275, 188);
            this.button_mod.Name = "button_mod";
            this.button_mod.Size = new System.Drawing.Size(75, 23);
            this.button_mod.TabIndex = 1;
            this.button_mod.Text = "수정";
            this.button_mod.UseVisualStyleBackColor = true;
            this.button_mod.Click += new System.EventHandler(this.button_mod_Click);
            // 
            // button_del
            // 
            this.button_del.Location = new System.Drawing.Point(356, 188);
            this.button_del.Name = "button_del";
            this.button_del.Size = new System.Drawing.Size(75, 23);
            this.button_del.TabIndex = 1;
            this.button_del.Text = "삭제";
            this.button_del.UseVisualStyleBackColor = true;
            this.button_del.Click += new System.EventHandler(this.button_del_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 12);
            this.label4.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 37);
            this.label2.TabIndex = 5;
            this.label2.Text = "카페 관리";
            // 
            // cafeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 229);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_del);
            this.Controls.Add(this.button_mod);
            this.Controls.Add(this.button_add);
            this.Controls.Add(this.listBox_cafe);
            this.Name = "cafeForm";
            this.Text = "cafeForm";
            this.Activated += new System.EventHandler(this.cafeForm_Activated);
            this.Load += new System.EventHandler(this.cafeForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.cafeForm_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_cafe;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button button_mod;
        private System.Windows.Forms.Button button_del;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
    }
}