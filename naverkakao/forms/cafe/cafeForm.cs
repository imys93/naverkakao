﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace naverkakao
{
    public partial class cafeForm : Form
    {
        public cafeForm()
        {
            InitializeComponent();
        }

        private void cafeForm_Load(object sender, EventArgs e)
        {
            this.refreshData();
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            cafeSubForm subFrm = new cafeSubForm();
            subFrm.Owner = this;
            subFrm.Show();
        }

        private void button_del_Click(object sender, EventArgs e)
        {
            CafeManager cm2 = new CafeManager();
            if (this.indexCheck())
            {
                cm2.removeCafe(this.listBox_cafe.SelectedIndex);
                this.listBox_cafe.Items.RemoveAt(this.listBox_cafe.SelectedIndex);
            }

        }

        public void refreshData()
        {
            CafeManager _cm = new CafeManager();

            this.listBox_cafe.Items.Clear();

            foreach (var c in _cm)
            {
                this.listBox_cafe.Items.Add(String.Format("{0}, {1}, 하단 양식 : {3}", c.cafe_id, c.cafe_name, c.categorys.Count, c.template));
            }

        }

        private void cafeForm_Paint(object sender, PaintEventArgs e)
        {
        }

        private void button_mod_Click(object sender, EventArgs e)
        {
            this.showModify();

        }

        private void cafeForm_Activated(object sender, EventArgs e)
        {
            this.refreshData();
        }

        private bool indexCheck()
        {
            if(this.listBox_cafe.SelectedIndex >= 0)
            {
                return true;
            }
            return false;
        }

        private void showModify()
        {
            if (this.indexCheck())
            {
                cafeSubForm subFrm = new cafeSubForm();
                subFrm.setModify(this.listBox_cafe.SelectedIndex);
                subFrm.Owner = this;
                subFrm.Show();
            }
        }

        private void listBox_cafe_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox_cafe_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.showModify();
        }
    }
}
