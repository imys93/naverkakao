﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace naverkakao
{
    public partial class cafeSubForm : Form
    {
        private bool isModify = false;
        private int modifyIndex = -1;
        public cafeSubForm()
        {
            InitializeComponent();
            
        }

        public void setModify(int cafeIndex)
        {
            CafeManager cm = new CafeManager();

            this.isModify = true;
            this.modifyIndex = cafeIndex;
            this.label_title.Text = "카페 수정";
            this.button_ok.Text = "수정";
            this.textBox_id.Text = cm[cafeIndex].cafe_id;
            this.label_desc.Text = cm[cafeIndex].cafe_name;
            this.textBox_id.Enabled = false;
            this.textBox_template.Text = cm[cafeIndex].template;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cafeSubForm_Load(object sender, EventArgs e)
        {

        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            CafeManager cm = new CafeManager();

            if (!this.isModify)
            {
                cm.addCafe(this.textBox_id.Text, this.textBox_template.Text);
                
            }
            else
            {
                if(this.modifyIndex >= 0)
                {
                    cm.setTemplate(this.modifyIndex, this.textBox_template.Text);
                }
            }

            this.Close();
        }

        private void textBox_id_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
