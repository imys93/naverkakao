﻿namespace naverkakao
{
    partial class kakaoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button_del = new System.Windows.Forms.Button();
            this.button_mod = new System.Windows.Forms.Button();
            this.button_add = new System.Windows.Forms.Button();
            this.listBox_kakao = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 24);
            this.label4.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(15, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(481, 72);
            this.label2.TabIndex = 11;
            this.label2.Text = "카카오스토리 관리";
            // 
            // button_del
            // 
            this.button_del.Location = new System.Drawing.Point(654, 367);
            this.button_del.Margin = new System.Windows.Forms.Padding(6);
            this.button_del.Name = "button_del";
            this.button_del.Size = new System.Drawing.Size(139, 46);
            this.button_del.TabIndex = 8;
            this.button_del.Text = "삭제";
            this.button_del.UseVisualStyleBackColor = true;
            this.button_del.Click += new System.EventHandler(this.button_del_Click);
            // 
            // button_mod
            // 
            this.button_mod.Location = new System.Drawing.Point(504, 367);
            this.button_mod.Margin = new System.Windows.Forms.Padding(6);
            this.button_mod.Name = "button_mod";
            this.button_mod.Size = new System.Drawing.Size(139, 46);
            this.button_mod.TabIndex = 9;
            this.button_mod.Text = "수정";
            this.button_mod.UseVisualStyleBackColor = true;
            this.button_mod.Click += new System.EventHandler(this.button_mod_Click);
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(353, 367);
            this.button_add.Margin = new System.Windows.Forms.Padding(6);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(139, 46);
            this.button_add.TabIndex = 10;
            this.button_add.Text = "추가";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // listBox_kakao
            // 
            this.listBox_kakao.FormattingEnabled = true;
            this.listBox_kakao.ItemHeight = 24;
            this.listBox_kakao.Location = new System.Drawing.Point(15, 155);
            this.listBox_kakao.Margin = new System.Windows.Forms.Padding(6);
            this.listBox_kakao.Name = "listBox_kakao";
            this.listBox_kakao.Size = new System.Drawing.Size(775, 196);
            this.listBox_kakao.TabIndex = 7;
            this.listBox_kakao.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBox_kakao_MouseDoubleClick);
            // 
            // kakaoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 443);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_del);
            this.Controls.Add(this.button_mod);
            this.Controls.Add(this.button_add);
            this.Controls.Add(this.listBox_kakao);
            this.Name = "kakaoForm";
            this.Text = "kakaoForm";
            this.Activated += new System.EventHandler(this.kakaoForm_Activated);
            this.Load += new System.EventHandler(this.kakaoForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_del;
        private System.Windows.Forms.Button button_mod;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.ListBox listBox_kakao;
    }
}