﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace naverkakao
{
    public partial class kakaoForm : Form
    {
        public kakaoForm()
        {
            InitializeComponent();
        }

        private void kakaoForm_Load(object sender, EventArgs e)
        {
            this.refreshData();
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            kakaoSubForm subFrm = new kakaoSubForm();
            subFrm.Owner = this;
            subFrm.Show();
        }

        private void button_del_Click(object sender, EventArgs e)
        {
            KakaoManager km = new KakaoManager();
            if (this.indexCheck())
            {
                int index = this.listBox_kakao.SelectedIndex;
                km.RemoveAt(index);
                this.listBox_kakao.Items.RemoveAt(index);
                km.saveData();
            }
        }

        public void refreshData()
        {
            KakaoManager km = new KakaoManager();

            this.listBox_kakao.Items.Clear();

            foreach (var k in km)
            {
                this.listBox_kakao.Items.Add(String.Format("{0}, 하단 양식 : {1}", k.title, k.template));
            }

        }

        private void button_mod_Click(object sender, EventArgs e)
        {
            this.showModify();

        }

        private void kakaoForm_Activated(object sender, EventArgs e)
        {
            this.refreshData();
        }

        private bool indexCheck()
        {
            if (this.listBox_kakao.SelectedIndex >= 0)
            {
                return true;
            }
            return false;
        }

        private void showModify()
        {
            if (this.indexCheck())
            {
                kakaoSubForm subFrm = new kakaoSubForm();
                subFrm.setModify(this.listBox_kakao.SelectedIndex);
                subFrm.Owner = this;
                subFrm.Show();
            }
        }

        private void listBox_kakao_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.showModify();
        }
    }
}
