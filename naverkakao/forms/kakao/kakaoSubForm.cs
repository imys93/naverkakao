﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace naverkakao
{
    public partial class kakaoSubForm : Form
    {
        private bool isModify = false;
        private int modifyIndex = -1;

        public kakaoSubForm()
        {
            InitializeComponent();
        }

        private void kakaoSubForm_Load(object sender, EventArgs e)
        {

        }

        public void setModify(int Index)
        {
            KakaoManager km = new KakaoManager();
            Model_kakao mk = km[Index];

            this.isModify = true;
            this.modifyIndex = Index;
            this.label_title.Text = "카카오스토리 수정";
            this.button_ok.Text = "수정";
            this.textBox_name.Text = mk.title;
            this.textBox_id.Text = mk.k_id;
            this.textBox_password.Text = mk.k_pw;
            this.textBox_template.Text = mk.template;
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            KakaoManager km = new KakaoManager();

            if (!this.isModify)
            {
                Model_kakao mk = new Model_kakao();
                mk.init();
                mk.title = this.textBox_name.Text;
                mk.k_id = this.textBox_id.Text;
                mk.k_pw = this.textBox_password.Text;
                mk.template = this.textBox_template.Text;
                km.Add(mk);
                km.saveData();
            }
            else
            {
                if (this.modifyIndex >= 0)
                {
                    Model_kakao mk = km[this.modifyIndex];
                    mk.title = this.textBox_name.Text;
                    mk.k_id = this.textBox_id.Text;
                    mk.k_pw = this.textBox_password.Text;
                    mk.template = this.textBox_template.Text;
                    km.saveData();
                    //pm.setTemplate(this.modifyIndex, this.textBox_body.Text);
                }
            }

            this.Close();
        }
    }
}
