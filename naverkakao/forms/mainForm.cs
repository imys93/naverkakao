﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace naverkakao
{
    public partial class mainForm : Form
    {
        const Boolean is_only_homepage_mode = true;
        const string homepage_id = "vovshop";
        delegate void AddItemCallback(string text);
        int errorCount = 0;
        //bool is_kakao_error = false;
        //bool end_kakao = false;
        bool is_running = false;
        private const string image_dir = ".\\data\\image\\";

        public mainForm()
        {
            InitializeComponent();
        }


        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            cafeForm cafeFrm = new cafeForm();

            cafeFrm.Show();


        }

        private void button3_Click(object sender, EventArgs e)
        {
            kakaoForm kakaoFrm = new kakaoForm();

            kakaoFrm.Show();
        }

        private void button_posts_Click(object sender, EventArgs e)
        {

            postForm postFrm = new postForm();
            postFrm.Show();
        }

        private void refreshCafe()
        {
            CafeManager cm = new CafeManager();
            cm.refreshCafes();
        }

        private bool checkPostCategory()
        {
            var cm = new CafeManager();
            var pm = new PostManager();

            foreach (var p in pm)
            {
                if(p.cafeToCategory == null)
                {
                    MessageBox.Show(string.Format("{0} 게시물에 카테고리가 설정되어 있지 않습니다. 카테고리 설정 후 작업을 시작해주세요.", p.title));
                    return false;
                }

                foreach (var c in cm)
                {
                    if(!p.cafeToCategory.ContainsKey(c.id))
                    {
                        MessageBox.Show(string.Format("{0} 게시물에 카테고리가 설정되어 있지 않습니다. 카테고리 설정 후 작업을 시작해주세요.", p.title));
                        return false;
                    }
                }

                foreach (var category in p.cafeToCategory)
                {
                    var q = cm.Where(c => c.id == category.Key);

                    //삭제된 카페에 대한 카테고리 정보가 남아있을 수 있으므로 색인하여 나오지 않는 경우는 패스
                    if(q.Count() > 0)
                    {
                        var cafe = q.First();
                        var q2 = cafe.categorys.Where(cate2 => cate2.c_id == category.Value);
                        if(q2.Count() == 0)
                        {
                            MessageBox.Show(string.Format("{0} 게시물에 카테고리가 설정되어 있지 않습니다. 카테고리 설정 후 작업을 시작해주세요.", p.title));
                            return false;
                        }
                    }

                }
            }
            return true;
        }

        private void start()
        {
            while (true)
            {
                if(is_only_homepage_mode)
                {
                    PrintLog("홈페이지 작업을 시작합니다.");
                    List<Model_post> h_pm = this.getRemainPosts(homepage_id);
                    myDriver h_m = new myDriver();
                    var h_login_result = h_m.shoplogin(!this.checkBox1.Checked);
                    if (!h_login_result.Success)
                    {

                        PrintLog(String.Format("오류발생 : {0}", h_login_result.ErrorMessage));
                        if (h_login_result.Exit == true)
                        {
                            h_m.kslogout();
                            h_m = null;

                            goto Exit2;
                        }

                        h_m.kslogout();
                        h_m = null;
                        goto Exit1;
                    }
                    else
                    {
                        PrintLog(String.Format("홈페이지 로그인 완료."));

                        if (h_pm.Count > 0)
                        {

                            PrintLog(String.Format("홈페이지에 올려야 할 게시물 {0}개를 찾았습니다.", h_pm.Count));


                            HistoryManager hm = new HistoryManager();

                            if (!is_running)
                            {
                                h_m.kslogout();
                                h_m = null;

                                goto Stop;
                            }
                            /*
                                try
                                {*/

                            int retryCount = 0;

                            for (int j = 0; j < h_pm.Count; j++)
                            {
                            Retry: if (!is_running)
                                {
                                    h_m.kslogout();
                                    h_m = null;

                                    goto Stop;
                                }
                                Thread.Sleep(1000);

                                Model_post p = h_pm[j];
                                PrintLog(String.Format("게시물 {0} 게시를 시작합니다.", p.title));

                                var result = h_m.shopUpload(p);

                                if (result.Success)
                                {
                                    hm.saveLog(homepage_id, p.id);
                                    PrintLog(String.Format("홈페이지에서 {0}를 게시하였습니다.", p.title));
                                }
                                else
                                {
                                    PrintLog("오류발생 : " + result.ErrorMessage);
                                    if (result.Exit == true)
                                    {
                                        h_m.kslogout();
                                        h_m = null;

                                        goto Exit2;
                                    }

                                    if (retryCount >= 3)
                                    {
                                        h_m.kslogout();
                                        h_m = null;


                                        goto Exit1;
                                    }
                                    else
                                    {
                                        PrintLog("오류발생으로 작성을 재시도합니다.");

                                        retryCount++;
                                        goto Retry;
                                    }
                                }

                                //게시 실패시 해당 게시물 다시 업로드 시도
                                //3번 실패시 빠져나와서 driver를 종료함
                            }
                            // PrintLog(String.Format("네이버 카페 {0}에 모든 게시물을 업로드하였습니다.", c.cafe_name));

                        }
                    }


                    PrintLog("홈페이지 작업을 완료했습니다.");
                }



                var km = new KakaoManager();
                var i = 0;
                //var isPause = false;

                
                PrintLog("카카오스토리 작업을 시작합니다.");

                while (i < km.Count)
                {
                    var k = km[i];

                    List<Model_post> pm = this.getRemainPosts(k.id);
                    i++;

                    if (pm.Count > 0)
                    {

                        PrintLog(String.Format("카카오스토리 계정 {0}에 올려야 할 게시물 {1}개를 찾았습니다.", k.title, pm.Count));


                        HistoryManager hm = new HistoryManager();
                        myDriver m = new myDriver();

                        if (!is_running)
                        {
                            m.kslogout();
                            m = null;

                            goto Stop;
                        }
                        /*
                            try
                            {*/
                        var login_result = m.kslogin(k, !this.checkBox1.Checked);
                        if (!login_result.Success)
                        {

                            PrintLog(String.Format("오류발생 : {0}", login_result.ErrorMessage));
                            m.kslogout();
                            m = null;
                            goto Exit1;
                        }
                        else
                        {
                            PrintLog(String.Format("카카오스토리 계정 {0}에 로그인 완료.", k.title));
                            int retryCount = 0;

                            for (int j = 0; j < pm.Count; j++)
                            {
                                Retry: if (!is_running)
                                {
                                    m.kslogout();
                                    m = null;

                                    goto Stop;
                                }
                                Thread.Sleep(2000);

                                Model_post p = pm[j];

                                var result = m.start2(p);

                                if (result.Success)
                                {
                                    hm.saveLog(k.id, p.id);
                                    //PrintLog(String.Format("카카오스토리 계정 {0}에 {1}를 게시하였습니다.", k.title, p.title));
                                }
                                else
                                {
                                    PrintLog("오류발생 : " + result.ErrorMessage);

                                    if (retryCount >= 3)
                                    {
                                        m.kslogout();
                                        m = null;

                                        goto Exit1;
                                    }
                                    else
                                    {
                                        PrintLog("오류발생으로 작성을 재시도합니다.");

                                        retryCount++;
                                        goto Retry;
                                    }

                                }

                                //게시 실패시 해당 게시물 다시 업로드 시도
                                //3번 실패시 빠져나와서 driver를 종료함
                            }
                            m.kslogout();
                            m = null;
                            PrintLog(String.Format("카카오스토리 계정 {0}에 모든 게시물을 업로드하였습니다.", k.title));

                        }
                    }
                }
                //done (위 loop를 정상적으로 빠져나오면 모든 작업을 완료한것.) 

                PrintLog("카카오스토리 작업을 완료했습니다.");

                CafeManager cm = new CafeManager();
                int ci = 0;

                PrintLog("네이버 카페 작업을 시작합니다.");
                myDriver m2 = new myDriver();
                var login_result2 = m2.nclogin(this.textBox_naver_id.Text, this.textBox_naver_pw.Text, !this.checkBox1.Checked);
                if (!login_result2.Success)
                {
                   
                    PrintLog(String.Format("오류발생 : {0}", login_result2.ErrorMessage));
                    if (login_result2.Exit == true)
                    {
                        m2.kslogout();
                        m2 = null;

                        goto Exit2;
                    }

                    m2.kslogout();
                    m2 = null;
                    goto Exit1;
                }
                else
                {
                    PrintLog(String.Format("네이버 계정에 로그인 완료."));
                    while (ci < cm.Count)
                {
                    var c = cm[ci];

                    List<Model_post> pm = this.getRemainPosts(c.id);
                    ci++;

                    if (pm.Count > 0)
                    {

                        PrintLog(String.Format("네이버 카페 {0}에 올려야 할 게시물 {1}개를 찾았습니다.", c.cafe_name, pm.Count));


                        HistoryManager hm = new HistoryManager();

                        if (!is_running)
                        {
                            m2.kslogout();
                            m2 = null;

                            goto Stop;
                        }
                        /*
                            try
                            {*/
                        
                            int retryCount = 0;

                            for (int j = 0; j < pm.Count; j++)
                            {
                                Retry: if (!is_running)
                                {
                                    m2.kslogout();
                                    m2 = null;

                                    goto Stop;
                                }
                                Thread.Sleep(10000);

                                Model_post p = pm[j];

                                var result = m2.start(p,c);

                                if (result.Success)
                                {
                                    hm.saveLog(c.id, p.id);
                                    PrintLog(String.Format("네이버 카페 {0}에 {1}를 게시하였습니다.", c.cafe_name, p.title));
                                }
                                else
                                {
                                    PrintLog("오류발생 : " + result.ErrorMessage);
                                    if (result.Exit == true)
                                    {
                                        m2.kslogout();
                                        m2 = null;

                                        goto Exit2;
                                    }

                                    if (retryCount >= 3)
                                    {
                                        m2.kslogout();
                                        m2 = null;

                                        
                                        goto Exit1;
                                    }
                                    else
                                    {
                                        PrintLog("오류발생으로 작성을 재시도합니다.");

                                        retryCount++;
                                        goto Retry;
                                    }
                                }

                                //게시 실패시 해당 게시물 다시 업로드 시도
                                //3번 실패시 빠져나와서 driver를 종료함
                            }
                            PrintLog(String.Format("네이버 카페 {0}에 모든 게시물을 업로드하였습니다.", c.cafe_name));

                        }
                    }
                }
                m2.kslogout();
                m2 = null;

                PrintLog("네이버 카페 작업을 완료했습니다.");

                goto Done;


                Exit1:
                if (errorCount >= 5)
                {
                    goto Exit2;
                }
                else
                {
                    PrintLog("오류로 인해 작업이 중단되었습니다. 처음부터 다시 시작합니다.");
                    errorCount++;
                }
                

            } // end of main while loop




            Exit2:
            PrintLog("복구할 수 없는 오류로 인해 작업을 종료합니다.");
            this.setStatus(false);
            return;

            Done:
            PrintLog("모든 작업이 완료되었습니다.");
            this.setStatus(false);
            return;

            Stop:
            PrintLog("작업을 중단하였습니다.");
            this.setStatus(false);
            return;

        }
        private void button8_Click(object sender, EventArgs e)
        {

        }

        private List<Model_post> getRemainPosts(string midea_id)
        {
            PostManager pm = new PostManager();
            HistoryManager hm = new HistoryManager();

            List<Model_post> queryed_pm = pm.Where(p => hm.Count(h => h.midea_id == midea_id && h.post_id == p.id) == 0).ToList();
            return queryed_pm;
        }
        private void refreshDisplay()
        {
            KakaoManager km = new KakaoManager();
            PostManager pm = new PostManager();
            CafeManager cm = new CafeManager();

            this.listBox1.Items.Clear();


            if(is_only_homepage_mode){
                int total_count = pm.Count;
                int did_count = pm.Count - this.getRemainPosts(homepage_id).Count;
                this.listBox1.Items.Add(String.Format("홈페이지 - {0}/{1}", did_count, total_count));
            }

            foreach (var c in cm)
            {
                int total_count = pm.Count;
                int did_count = pm.Count - this.getRemainPosts(c.id).Count;
                this.listBox1.Items.Add(String.Format("카페/{0} - {1}/{2}", c.cafe_name, did_count, total_count));
            }

            foreach (var k in km)
            {
                int total_count = pm.Count;
                int did_count = pm.Count - this.getRemainPosts(k.id).Count;
                this.listBox1.Items.Add(String.Format("카카오/{0} - {1}/{2}", k.title, did_count, total_count));
            }


            /*
            this.listBox2.Items.Clear();
            foreach (var p in new PostManager())
            {
                string str = p.title + ((p.onlyFriend == true) ? "(친구공개)" : "");
                this.listBox2.Items.Add(str);
            }*/
        }

        private void mainForm_Activated(object sender, EventArgs e)
        {
            this.refreshDisplay();

        }

        private void setStatus(bool status)
        {
            this.is_running = status;
            string button_text = (status) ? "작업 종료" : "작업 시작";
            if (this.button_toggle.InvokeRequired)
            {
                this.button_toggle.Invoke(new MethodInvoker(delegate {
                    this.button_toggle.Text = button_text;
                }));
            }
            else
            {
                this.button_toggle.Text = button_text;
            }
        }

        private void PrintLog(string s)
        {
            if (this.listBox3.InvokeRequired)
            {
                this.listBox3.Invoke(new MethodInvoker(delegate {
                    this.listBox3.Items.Add(s);
                    this.listBox3.SelectedIndex = this.listBox3.Items.Count-1;

                }));
            }
            else
            {
                this.listBox3.Items.Add(s);
                this.listBox3.SelectedIndex = this.listBox3.Items.Count-1;
            }



        }
        
        private void mainForm_Load(object sender, EventArgs e)
        {
            System.IO.Directory.CreateDirectory("./data");
            System.IO.Directory.CreateDirectory("./data/image");
            var cm = new CafeManager();
            cm.refreshCafes();

           var sm = new ShopCategoryManager();
            sm.refreshData();

            var sitem = new SiteManager();
            PrintLog(sitem.getSiteURL());

        }

        private void button_toggle_Click(object sender, EventArgs e)
        {
            if (is_running)
            {
                this.setStatus(false);
            }
            else
            {
                var cm = new CafeManager();
                cm.refreshCafes();

                if(this.checkPostCategory())
                {
                    this.setStatus(true);
                    errorCount = 0;
                    //is_kakao_error = false;
                    new Thread(new ThreadStart(start)).Start();
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("확인을 누르시면 모든 게시물이 삭제됩니다. 계속하시겠습니까?", "초기화", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {

                PostManager pm = new PostManager();
                pm.RemoveRange(0, pm.Count);
                pm.saveData();

                System.IO.DirectoryInfo di = new DirectoryInfo(image_dir);

                try
                {
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    this.refreshDisplay();
                }
                catch (Exception)
                {
                    MessageBox.Show("모든 사진을 삭제하지 못했습니다. \n 게시물 관리 윈도우를 닫은 후 다시 시도해주세요.");
                    return;
                }

                MessageBox.Show("삭제가 완료되었습니다.");
            }



            //this.refreshData();

        }

        private void mainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}