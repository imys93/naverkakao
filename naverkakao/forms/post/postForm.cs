﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace naverkakao
{
    public partial class postForm : Form
    {
        bool isModify = false;
        int modifyIndex = 0;
        public postForm()
        {
            InitializeComponent();
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            postSubForm subFrm = new postSubForm();
            subFrm.Owner = this;
            subFrm.Show();
            this.isModify = false;
            modifyIndex = 0;
        }

        private void postForm_Load(object sender, EventArgs e)
        {
            this.refreshData();
        }

        public void refreshData()
        {
            PostManager _pm = new PostManager();

            this.listBox_post.Items.Clear();

            foreach (var p in _pm)
            {
                string str = p.title + ((p.onlyFriend == true) ? "(친구공개)" : "");
                this.listBox_post.Items.Add(str);
            }

            if(isModify)
            {
                this.listBox_post.SelectedIndex = this.modifyIndex;
            }
            else
            {
                this.listBox_post.SelectedIndex = this.listBox_post.Items.Count - 1;
            }

        }

        private void postForm_Activated(object sender, EventArgs e)
        {
            this.refreshData();
        }

        private bool indexCheck()
        {
            if (this.listBox_post.SelectedIndex >= 0)
            {
                return true;
            }
            return false;
        }
        private void showModify()
        {
            if (this.indexCheck())
            {
                this.isModify = true;
                this.modifyIndex = this.listBox_post.SelectedIndex;
                postSubForm subFrm = new postSubForm();
                subFrm.setModify(this.listBox_post.SelectedIndex);
                subFrm.Owner = this;
                subFrm.Show();
            }
        }

        private void button_mod_Click(object sender, EventArgs e)
        {
            this.showModify();
        }

        private void listBox_post_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.showModify();
        }

        private void button_del_Click(object sender, EventArgs e)
        {
            PostManager pm = new PostManager();
            if (this.indexCheck())
            {
                var index = this.listBox_post.SelectedIndex;
                pm.RemoveAt(index);
                this.listBox_post.Items.RemoveAt(index);
                pm.saveData();
            }
        }

        private void listBox_post_SelectedIndexChanged(object sender, EventArgs e)
        {
            PostManager pm = new PostManager();
            if (this.indexCheck())
            {
                this.label1.Text = "";

                var index = this.listBox_post.SelectedIndex;
                for (int i = 1; i < 6; i++)
                {
                    this.Controls["pictureBox" + i].BackgroundImage = null;

                }
                this.label1.Text = pm[index].content;

                int j=1;
                foreach (var path in pm[index].imagepaths)
                {
                    if(j > 5)
                    {
                        goto ExitFor; 
                    }
                    this.Controls["pictureBox" + j].BackgroundImage = Image.FromFile(path);
                    j++;
                }
                ExitFor:
                return;
            }

        }
    }
}
