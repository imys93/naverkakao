﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace naverkakao
{
    public partial class postSubForm : Form
    {
        private const string image_dir = ".\\data\\image\\";


        private bool isModify = false;
        private int modifyIndex = -1;
        private List<string> imagepaths = new List<string>();

        public postSubForm()
        {
            InitializeComponent();
        }

        public void setModify(int Index)
        {
            PostManager pm = new PostManager();
            Model_post p = pm[Index];

            this.isModify = true;
            this.modifyIndex = Index;
            this.label_title.Text = "글 수정";
            this.button_ok.Text = "수정";
            this.textBox_title.Text = p.title;
            this.textBox_body.Text = p.content;
            this.imagepaths = p.imagepaths;
            this.checkBox_onlyFriend.Checked = p.onlyFriend;
            
        }


        private void resetForm()
        {
            this.textBox_title.Text = "";
            this.textBox_body.Text = "";
            this.imagepaths.Clear();
            this.imageList1.Images.Clear();
            this.listView1.Items.Clear();
            this.comboBox1.Items.Clear();
            this.checkBox_onlyFriend.Checked = true;
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                this.dataGridView1.Rows[i].Cells[1].Value = null;
            }
        }

        private bool checkGridView()
        {
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                if (((DataGridViewComboBoxCell)this.dataGridView1.Rows[i].Cells[1]).Value == null)
                {
                    return false;
                }
            }
            return true;
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            if(!this.checkGridView())
            {
                MessageBox.Show("카테고리를 설정해주세요.");
                return;
            }
            var ctc = new Dictionary<string, string>();
            //ctc[new CafeManager()[0].id] = new CafeManager()[0].categorys[2].c_id;
            var cm = new CafeManager();
            
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                var row = this.dataGridView1.Rows[i];
                string cafe_name = row.Cells[0].Value.ToString();
                string cafe_id = cm[i].id;

                //            List<Model_post> queryed_pm = pm.Where(p => hm.Count(h => h.midea_id == midea_id && h.post_id == p.id) == 0).ToList();

                string category_name = row.Cells[1].Value.ToString();
                string category_id = cm[i].categorys.Where(c => c.category_name == category_name).First().c_id;
                ctc.Add(cafe_id, category_id);
                Console.WriteLine(cafe_id, category_id);
            }

            PostManager pm = new PostManager();
            ShopCategoryManager scm = new ShopCategoryManager();

            if (!this.isModify)
            {
                Model_post mp = new Model_post();
                mp.init();
                mp.title = this.textBox_title.Text;
                mp.content = this.textBox_body.Text;
                mp.imagepaths = this.imagepaths;
                mp.onlyFriend = this.checkBox_onlyFriend.Checked;
                mp.cafeToCategory = ctc;
                mp.shop_cate_id = scm[this.comboBox1.SelectedIndex].c_id;
                pm.Add(mp);
                pm.saveData();
                this.resetForm();
                ((postForm)this.Owner).refreshData();
            }
            else
            {
                if (this.modifyIndex >= 0)
                {
                    pm[this.modifyIndex].title = this.textBox_title.Text;
                    pm[this.modifyIndex].content = this.textBox_body.Text;
                    pm[this.modifyIndex].imagepaths = this.imagepaths;
                    pm[this.modifyIndex].onlyFriend = this.checkBox_onlyFriend.Checked;
                    pm[this.modifyIndex].cafeToCategory = ctc;
                    pm[this.modifyIndex].shop_cate_id = scm[this.comboBox1.SelectedIndex].c_id; 

                    pm.saveData();
                    ((postForm)this.Owner).refreshData();
                    this.label_title.Text = "글 수정 - 수정완료";
                    ((postForm)this.Owner).refreshData();

                    //pm.setTemplate(this.modifyIndex, this.textBox_body.Text);
                }
            }


        }

        private void postSubForm_Load(object sender, EventArgs e)
        {
            this.listView1.LargeImageList.ImageSize = new Size(96, 96);
            this.refreshDisplay();



        }

        private void refreshDisplay()
        {
            this.imageList1.Images.Clear();
            this.listView1.Items.Clear();

            for (int i = 0; i < this.imagepaths.Count(); i++)
            {
                this.imageList1.Images.Add(Image.FromFile(this.imagepaths[i]));
            }

            for (int j = 0; j < this.imageList1.Images.Count; j++)
            {
                this.listView1.Items.Add("",j);
            }

            ShopCategoryManager scm = new ShopCategoryManager();
            for (int i1 = 0; i1 < scm.Count; i1++)
            {
                this.comboBox1.Items.Add(scm[i1].category_name);
            }
            this.comboBox1.SelectedIndex = 0;

            this.dataGridView1.Rows.Clear();
            CafeManager cm = new CafeManager();

            for (int i = 0; i < cm.Count; i++)
            {
                dataGridView1.Rows.Add(cm[i].cafe_name, null);
                var cell = new DataGridViewComboBoxCell()
                {
                    DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                };

                
                

                for (int j = 0; j < cm[i].categorys.Count; j++)
                {
                    cell.Items.Add(cm[i].categorys[j].category_name);

                }
                dataGridView1[1, i] = cell;
            }

            if (modifyIndex != -1)
            {
                var pm = new PostManager();
                var p = pm[modifyIndex];
                foreach (DataGridViewRow row in this.dataGridView1.Rows)
                {
                    string cafe_name = row.Cells[0].Value.ToString();

                    var c = cm.Where(_c => _c.cafe_name == cafe_name).First();
                    string cafe_id = c.id;
                    if (p.cafeToCategory != null)
                    {
                        if (p.cafeToCategory.ContainsKey(cafe_id))
                        {
                            var q = c.categorys.Where(category => category.c_id == p.cafeToCategory[cafe_id]);
                            
                            if (q.Count() > 0)
                            {
                                string category_name = q.First().category_name;
                                row.Cells[1].Value = category_name;

                            }
                        }
                    }
                  

                    //cm[i].categorys.Where(c => c.category_name == category_name).First().c_id;

                    //string cafe_id = ;
                    //p.cafeToCategory.ContainsKey
                }

                if (p.shop_cate_id != null)
                {
                    var q2 = scm.Where(category => category.c_id == p.shop_cate_id);

                    if (q2.Count() > 0)
                    {
                        string category_name = q2.First().category_name;
                        this.comboBox1.Text = category_name;
                    }
                }
            }
        }

        private string GetRandomFilename(int index)
        {
            DateTime d = DateTime.Now;

            string timestring = d.ToString("ddMMyyyyHHmmss");
            string randomIntString = new Random(Guid.NewGuid().GetHashCode()).Next(1000000).ToString();
            return String.Format("{0}_{1}_{2}", timestring, randomIntString, index);
        }

        private void save_photos(string[] files)
        {

            for (int i = 0; i < files.Count(); i++)
            {
                string old_filename = files[i];

                string filetype = Path.GetExtension(old_filename).ToLower();
                if (filetype == ".png" || filetype == ".jpg" || filetype == ".bmp" || filetype == ".gif")
                {
                }
                else
                { 
                    goto Exit_upload_photo;
                }

                string new_filename = image_dir + GetRandomFilename(i) + filetype;

                try
                {
                    File.Copy(old_filename, new_filename);
                    this.imagepaths.Add(new_filename);
                    //Console.WriteLine(new_filename);
                }
                catch (IOException)
                {
                    goto Exit_upload_photo;
                }
            }

            this.refreshDisplay();
            return;

        Exit_upload_photo:
            MessageBox.Show("사진을 첨부하지 못하였습니다.");
            this.imagepaths.Clear();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "이미지 파일(*.BMP;*.JPG;*.GIF;*.PNG;)|*.BMP;*.JPG;*.GIF;*.PNG";
            dialog.Multiselect = true;
            if(dialog.ShowDialog() == DialogResult.OK)
            {

                this.save_photos(dialog.FileNames);


            }
        }

        private void listView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;

        }

        private void listView1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            this.save_photos(files);
        }

        private void button_reset_Click(object sender, EventArgs e)
        {
            this.imagepaths = new List<string>();
            this.refreshDisplay();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                this.resetForm();
                this.setModify(this.modifyIndex + 1);
                this.refreshDisplay();

            }
            catch (Exception)
            {
                MessageBox.Show("목록의 끝입니다.");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                this.resetForm();
                this.setModify(this.modifyIndex - 1);
                this.refreshDisplay();

            }
            catch (Exception)
            {
                MessageBox.Show("목록의 끝입니다.");
            }

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
