﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace naverkakao
{
    public class Model_cafe
    {
        public string id;
        public string cafe_id;
        public string cafe_code;
        public string template;
        public string cafe_name;
        public List<Model_category> categorys;

        public Model_cafe()
        {
            
        }

        public void init(int index, string cafe_id, string templateString)
        {
            Guid guid = Guid.NewGuid();
            this.id = guid.ToString();

            this.cafe_id = cafe_id;
            this.setTemplate(templateString);
            this.updateInfo();
        }
        public void updateInfo()
        {
            CafeCrawler crawler = new CafeCrawler(this.cafe_id);
            this.cafe_name = crawler.getCafeName();
            this.cafe_code = crawler.getCafeCode();
            this.categorys = crawler.getCategorys();
        }

        public void setTemplate(string templateString)
        {
            this.template = templateString;
            CafeCrawler crawler = new CafeCrawler(this.cafe_id);
            this.cafe_name = crawler.getCafeName();
            this.cafe_code = crawler.getCafeCode();
            this.categorys = crawler.getCategorys();
        }
    }
}
