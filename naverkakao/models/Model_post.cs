﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace naverkakao
{
    public class Model_post
    {
        public string id;
        public string title;
        public string content;
        public List<string> imagepaths = new List<string>();
        public bool onlyFriend;
        public Dictionary<string, string> cafeToCategory;
        public string shop_cate_id;

        public Model_post()
        {
            
        }

        public void init()
        {
            Guid guid = Guid.NewGuid();
            this.id = guid.ToString();
        }
        
    }
}
