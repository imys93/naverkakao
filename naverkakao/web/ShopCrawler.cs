﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.Web;
using System.IO;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace naverkakao
{
    class ShopCrawler
    {
        private string source;

        public ShopCrawler()
        {
            this.source = this.getHttp("http://poten.pe.kr/xe/index.php");



        }

        //카페 유효성 검증함수 필요함, GUI에서 바로 유효성검사 후에 등록절차 진행
        public string getHttp(string url)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string status = response.StatusCode.ToString();

            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("utf-8"));
            string text = reader.ReadToEnd();
            response.Close();

            return text;
        }



        public List<Model_category> getCategorys()
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(this.source);
            //div[@class='bubble-title'

            //search.menuid=5&search.boardtype=L

            //            HtmlNodeCollection icons = doc.DocumentNode.SelectNodes("//i[@class='icon_board b1']");

            HtmlNodeCollection dom_boards = doc.DocumentNode.SelectNodes("//li[@class='category_item']/a[contains(@href, 'index.php?mid=')]");

            List<Model_category> categorys = new List<Model_category>();
            foreach (HtmlNode li in dom_boards)
            {
              
                    string board_url = li.Attributes["href"].Value;
                    //string pattern = @"menuid=(\D+)&";
                    string board_id = Regex.Split(board_url, "mid=")[1];

                    string board_name = li.InnerHtml;

                    Model_category cg = new Model_category();
                    cg.c_id = board_id;
                    cg.category_name = board_name;
                    categorys.Add(cg);

                    Console.WriteLine(board_id);
                    Console.WriteLine(li.InnerHtml);
                



                //                HtmlNode board_name = li.SelectSingleNode("../span[@class='tit_board']/span[@class='tit_text']");
                //              Console.WriteLine(WebUtility.HtmlDecode(board_name.InnerText.Trim()));
            }

            return categorys;
        }

        public string getCafeName()
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(this.source);
            string title = doc.DocumentNode.SelectSingleNode("//title").InnerText;

            string cafeName = Regex.Split(title, " :")[0];
            return cafeName;
        }

        public string getCafeCode()
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(this.source);

            try
            {

                string cafe_full_url = doc.DocumentNode.SelectSingleNode("//a[@name='myCafeUrlLink']").Attributes["href"].Value;

                string cafeCode = Regex.Split(cafe_full_url, "=")[1];
                return cafeCode;
            }
            catch (NullReferenceException)
            {

                //throw;
            }
            return "";
        }
    }
}
