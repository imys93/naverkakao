﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Drawing.Imaging;

namespace naverkakao
{
    class myDriver
    {
        public class Result
        {
            public bool Success { get; set; }
            public string id { get; set; }
            public string ErrorMessage { get; set; }
            public bool Exit { get; set; }
        }

        IWebDriver driver;
        string template_string;

        public myDriver()
        {

            
        }

        public void ScreenShot()
        {
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
            
            ss.SaveAsFile("./data/shots/" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpg", ScreenshotImageFormat.Jpeg);
        }

        private string replaceNewLine(string str)
        {
            return str.Replace("\n", "</br>");
        }


        public Result ResultWithSuccess(string id)
        {
            Result result = new Result();
            result.Success = true;
            result.id = id;

            return result;
        }

        public Result ResultWithError(string errorMessage, bool exit)
        {
            Result result = new Result();
            result.Success = false;
            result.ErrorMessage = errorMessage;
            result.Exit = exit;

            return result;
        }
        public void processKill()
        {
            try
            {
                Process[] processList = Process.GetProcessesByName("chromedriver");
                foreach (var p in processList)
                {
                    p.Kill();
                }

                Process[] processList2 = Process.GetProcessesByName("chrome");
                foreach (var p in processList2)
                {
                    p.Kill();
                }

            }
            catch (Exception)
            {

            }
        }

        public Result shoplogin(bool isHeadless)
        {
            processKill();
            var sitem = new SiteManager();


            ChromeDriverService driverService = ChromeDriverService.CreateDefaultService();
            //driverService.HideCommandPromptWindow = true;
            ChromeOptions option = new ChromeOptions();
            if (isHeadless)
            {
                option.AddArgument("--headless");
            }

            try
            {
                driver = new ChromeDriver(driverService, option);


            }
            catch (Exception)
            {
                return ResultWithError("로드안됨, 반복해서 이 에러가 발생할 경우 재부팅 후 사용해주세요.", false);

            }
            driver.Manage().Window.Size = new Size(1920, 1080); // Size is a type in assembly "System.Drawing"
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);

            //this.template_string = k.template;
            WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(5));

            driver.Url = string.Format("http://{0}/xe/index.php?act=dispMemberLoginForm", sitem.getSiteURL());


            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("uid")));
            driver.FindElement(By.Id("uid")).SendKeys("vovshop@admin.com");
            driver.FindElement(By.Id("upw")).SendKeys("vovshop1020!\n");
            //driver.FindElement(By.ClassName("btn_login")).Click();

            try
            {
                //wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("lab_login")));
                wait.Until(ExpectedConditions.UrlContains(string.Format("http://{0}/xe/", sitem.getSiteURL())));

                return ResultWithSuccess("1");
            }
            catch (WebDriverTimeoutException)
            {
                //                                PrintLog("카카오스토리 서버에 접속할 수 없습니다. 인터넷에 연결되어 있지 않거나, IP를 변경해야 합니다.");

                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("desc_error")));
                return ResultWithError("비번틀림", true);

                //Console.WriteLine("비번틀림");
            }


        }

        public Result kslogin(Model_kakao k, bool isHeadless)
        {
            processKill();

            ChromeDriverService driverService = ChromeDriverService.CreateDefaultService();
            //driverService.HideCommandPromptWindow = true;
            ChromeOptions option = new ChromeOptions();
            if(isHeadless)
            {
                option.AddArgument("--headless");
            }

            try
            {
                driver = new ChromeDriver(driverService, option);


            }
            catch (Exception)
            {
                return ResultWithError("로드안됨, 반복해서 이 에러가 발생할 경우 재부팅 후 사용해주세요.", false);

            }
            driver.Manage().Window.Size = new Size(1920, 1080); // Size is a type in assembly "System.Drawing"
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);

            this.template_string = k.template;
            WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(5));

            driver.Url = "http://kakaostory.com";

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("id_email_2")));
            driver.FindElement(By.Id("id_email_2")).SendKeys(k.k_id);
            driver.FindElement(By.Id("id_password_3")).SendKeys(k.k_pw);
            driver.FindElement(By.ClassName("btn_g submit")).Click();

            try
            {
                //wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("lab_login")));
                wait.Until(ExpectedConditions.UrlContains("https://story.kakao.com/"));
                return ResultWithSuccess("1");
            }
            catch (WebDriverTimeoutException)
            {
                //                                PrintLog("카카오스토리 서버에 접속할 수 없습니다. 인터넷에 연결되어 있지 않거나, IP를 변경해야 합니다.");

                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("desc_error")));
                return ResultWithError("비번틀림", true);

                //Console.WriteLine("비번틀림");
            }


        }

        public void kslogout()
        {
            if(driver!=null)
                driver.Quit();
        }

        public Result start2(Model_post p)
        {

            try
            {
                
                WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(5));
                driver.Url = "http://kakaostory.com";

                //driver.Url = "http://cafe.naver.com/ArticleWrite.nhn?clubid=29309831&menuid=1&boardtype=L&m=write";
                /*
                                Console.WriteLine(driver.Url);
                                if (driver.Url.IndexOf("login/kakaostory") > 0)
                                {
                                    Console.WriteLine("카카오스토리 로그인 에러");
                                    driver.Quit();
                                    return;
                                }
                                else {

                                }
                                */

                //글쓰기 페이지 대기

                //wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("​editable_clone")));


                /*
                //사진 업로드
                driver.FindElement(By.ClassName("ico_pic")).Click();
                wait.Until<bool>((d) => { return (d.WindowHandles.Count > 1); });
                driver.SwitchTo().Window(driver.WindowHandles.ToList().Last());

                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("npe_alert_btn_close")));
                driver.FindElement(By.ClassName("npe_alert_btn_close")).Click();


                */
                //파일첨부


                wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".link_menu._changeMode")));

                driver.FindElements(By.CssSelector(".link_menu._changeMode"))[0].Click();
                wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".media_wrap.photo")));

                var aaa = driver.FindElements(By.CssSelector("._photoFileInput.bg_file_upload"));
                Console.WriteLine(aaa.Count);
                List<string> paths = p.imagepaths;
                for (int i = 0; i < paths.Count(); i++)
                {

                    paths[i] = Path.Combine(Directory.GetCurrentDirectory(), paths[i]);
                }
                string imagePathsString = string.Join(" \n ", paths);
                Console.WriteLine(imagePathsString);
                aaa[0].SendKeys(imagePathsString);

                //공개설정
                wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("._permissionBtn.bn_open")));
                driver.FindElement(By.CssSelector("._permissionBtn.bn_open")).Click();
                wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".ly.open_layer")));

                if (p.onlyFriend)
                {
                    driver.FindElements(By.CssSelector("._permissionItem"))[1].Click();
                }
                else
                {
                    driver.FindElements(By.CssSelector("._permissionItem"))[0].Click();

                }


                string header_string = p.content;
                //                string imgSource = driver.FindElement(By.Id("textbox")).GetAttribute("value");
                string footer_string = "\n\n" + this.template_string;

                string post_body = (header_string + footer_string);
                //Console.WriteLine(post_body);

                //driver.FindElement(By.Id("textbox")).Clear();
                //내용길면 여기서 브레이크포인트
                //driver.FindElement(By.Id("textbox")).SendKeys(post_body);


                


                //게시글 작성


                //IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                wait.Until(ExpectedConditions.ElementIsVisible(By.Id("contents_write")));
                //js.ExecuteScript("​document.getElementById('contents_write').innerHTML = '123'", post_body);
                //String keysPressed = Keys.Control + Keys.Enter;

                driver.FindElement(By.Id("contents_write")).SendKeys(post_body);
                wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("._postBtn.btn_com.btn_or")));
                driver.FindElement(By.CssSelector("._postBtn.btn_com.btn_or")).Click();
                //driver.FindElement(By.Id("contents_write")).SendKeys(keysPressed);

                try
                {
                    wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("contents_write")));
                    return ResultWithSuccess("1");
                }
                catch (WebDriverTimeoutException)
                {
                    this.ScreenShot();

                    var toast_popup = driver.FindElement(By.CssSelector(".toast_popup.cover_content.cover_center"));
                    var toast_message = toast_popup.FindElement(By.ClassName("txt"));
                    return ResultWithError("에러" + toast_message.Text, false);
                    //driver.Quit();
                }
                //return(in try)

                //driver.Quit();
                // js.ExecuteScript("​document.getElementsByClassName('_postBtn')[0].click()");
               // wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("*[contains(@class, '_postBtn')]")));
               
                //a[contains(@href, 'search.menuid=')]
              
                //string done_url = driver.Url;

                /*
                if (done_url.IndexOf("articleid=") >= 0)
                {
                    string s = Regex.Split(driver.Url, "articleid=")[1].Split('%')[0];
                    Console.WriteLine("게시글 등록 완료 글번호 : {0}", s);
                    driver.Close();
                    driver.Quit();
                }
                else
                {

                    //작성 후 에러메시지 감지
                    string tmp = driver.Url;
                    IAlert alert = driver.SwitchTo().Alert();
                    if (alert != null)
                    {
                        Console.WriteLine("alertstring : {0}", alert.Text);
                        alert.Accept();
                        driver.Quit();
                        return;
                        //  driver.Close();
                    }
                }*/

            } //end try

            catch (InvalidOperationException)
            {
                return ResultWithError("게시물 내용에 입력할 수 없는 문자가 존재합니다.", true);
            }
            catch (NoSuchElementException)
            {
                return ResultWithError("NoSuchElementException", false);
            }
            catch (WebDriverTimeoutException)
            {
                return ResultWithError("타임아웃", false);
            }
            catch (NoAlertPresentException)
            {
                return ResultWithError("예기치 않은 Alert 발생으로 종료되었습니다. ", false);
            }
            catch (UnhandledAlertException)
            {
                IAlert alert = driver.SwitchTo().Alert();

                try
                {

                        alert.Accept();
                        return ResultWithError("Alert : " + alert.Text, false);
                }
                catch (NoAlertPresentException)
                {
                    return ResultWithError("Alert이 null 입니다.", false);
                }


            }
            catch (WebDriverException)
            {
                return ResultWithError("도중에 webdriver가 종료되었습니다.", true);
            }
        }

        public Result nclogin(string id, string pw, bool isHeadless)
        {
            processKill();

            ChromeDriverService driverService = ChromeDriverService.CreateDefaultService();
            //driverService.HideCommandPromptWindow = true;
            ChromeOptions option = new ChromeOptions();
            if (isHeadless)
            {
                option.AddArgument("--headless");
            }

            try
            {
                driver = new ChromeDriver(driverService, option);


            }
            catch (Exception)
            {
                return ResultWithError("로드안됨", false);

            }
            driver.Manage().Window.Size = new Size(1920, 1080); // Size is a type in assembly "System.Drawing"
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);

            WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(30));

            driver.Url = "https://nid.naver.com/nidlogin.login";

            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("id")));
            driver.FindElement(By.Id("id")).SendKeys(id);
            driver.FindElement(By.Id("pw")).SendKeys(pw);
            driver.FindElement(By.ClassName("btn_global")).Click();
            try
            {
                //wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("lab_login")));
                wait.Until(ExpectedConditions.UrlContains("https://www.naver.com/"));
                return ResultWithSuccess("1");
            }
            catch (WebDriverTimeoutException)
            {
                //                                PrintLog("카카오스토리 서버에 접속할 수 없습니다. 인터넷에 연결되어 있지 않거나, IP를 변경해야 합니다.");
                try
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id("err_common")));
                    return ResultWithError("비번틀림", true);

                }
                catch (Exception)
                {
                    return ResultWithError("로그인 할 수 없습니다.(로그인보안문자)", true);

                }

                //Console.WriteLine("비번틀림");
            }
        }

        public string checkAlert()
        {
            try
            {
                System.Threading.Thread.Sleep(100);
                IAlert alert = driver.SwitchTo().Alert();
                if (alert != null)
                {
                    alert.Accept();
                    return "alertstring : " + alert.Text;

                    //  driver.Close();
                }
                else
                {
                    return "";
                }

            }
            catch (Exception)
            {
                return "";
            }

        }

        public Result start(Model_post p, Model_cafe c)
        {
            
            try
            {
                this.template_string = c.template;
                
                WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(30));
                
                driver.Url = string.Format("http://cafe.naver.com/ArticleWrite.nhn?clubid={0}&menuid={1}&boardtype=L&m=write", c.cafe_code, p.cafeToCategory[c.id]);

                if(driver.Url.IndexOf("cafe.naver.com/ArticleWrite.nhn?") < 0)
                {
                    if (driver.Url.IndexOf("iframe_url=/ArticleWrite.nhn") > 0)
                    {
                        return ResultWithError("게시물 등록 실패(글쓰기 권한 없음)", true);
                    }
                }

                Console.WriteLine(driver.Url);

                //글쓰기 페이지 대기
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("step01")));

                //게시판 선택됐는지 체크
                string selected_board = driver.FindElement(By.ClassName("step01")).GetAttribute("value");
                if (selected_board == "-1")
                {
                    return ResultWithError("게시물 등록 실패(카테고리 지정안됨 또는 카테고리 삭제됨", true);
                }

                //사진 업로드
                if(p.imagepaths.Count > 0)
                { 
                    driver.FindElement(By.ClassName("ico_pic")).Click();
                    wait.Until<bool>((d) => { return (d.WindowHandles.Count > 1); });
                    driver.SwitchTo().Window(driver.WindowHandles.ToList().Last());

                    try
                    {
                        wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("npe_popup_btn")));
                        driver.FindElement(By.ClassName("npe_popup_btn")).Click();
                    }
                    catch (Exception)
                    {
                        
                    }

                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("npe_alert_btn_close")));
                    driver.FindElement(By.ClassName("npe_alert_btn_close")).Click();



                    //파일첨부
                    List<string> paths = p.imagepaths;
                    for (int i = 0; i < paths.Count(); i++)
                    {

                        paths[i] = Path.Combine(Directory.GetCurrentDirectory(), paths[i]);
                    }
                    string imagePathsString = string.Join(" \n ", paths);
                    Console.WriteLine(imagePathsString);

                    driver.FindElement(By.Id("pc_image_file")).SendKeys(imagePathsString);
                    //wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("npu_image_list_thumb")));
                    //Threading.Thread.Sleep(2000);
                    wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("npe_alert")));
                    driver.FindElement(By.ClassName("npu_btn_submit")).Click();


                    //제목, 본문 입력(사진 업로드 후 아래부분 딜레이 안주면 오류남)

                    wait.Until<bool>((d) => { return (d.WindowHandles.Count == 1); });

                    driver.SwitchTo().Window(driver.WindowHandles.ToList().First());
                }


                System.Threading.Thread.Sleep(2000);
                //driver.FindElement(By.Id("elHtmlMode")).Click();
                System.Threading.Thread.Sleep(100);

                //제목 입력
                driver.FindElement(By.Id("subject")).Clear();
                driver.FindElement(By.Id("subject")).SendKeys(p.title);

                //내용 입력

                //driver.SwitchTo().Frame()
                var iframe1 = driver.FindElement(By.Id("editer_border")).FindElement(By.TagName("iframe"));
                driver.SwitchTo().Frame(iframe1);

                

                string header_string = p.content;
                string imgSource = driver.FindElement(By.TagName("body")).GetAttribute("innerHTML");
                string footer_string = "\n\n" + c.template;

                //driver.FindElement(By.TagName("body")).

                //Console.WriteLine(imgSource);
                string post_body = this.replaceNewLine(header_string + footer_string) + imgSource;
                //Console.WriteLine(post_body);

                //driver.FindElement(By.Id("textbox")).Clear();
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;

                var bodyElement = driver.FindElement(By.TagName("body"));

                js.ExecuteScript("var ele=arguments[0]; ele.innerHTML = arguments[1];",bodyElement, post_body);

                //내용 입력 마무리
                //driver.FindElement(By.Id("elHtmlMode")).Click();

                driver.SwitchTo().DefaultContent();

                //게시글 작성
                driver.FindElement(By.Id("cafewritebtn")).Click();
                System.Threading.Thread.Sleep(2000);


                string done_url = driver.Url;

                if (done_url.IndexOf("articleid=") >= 0)
                {
                    string s = Regex.Split(driver.Url, "articleid=")[1].Split('%')[0];
                    return ResultWithSuccess(s);
                }
                /*
                else
                {

                    //작성 후 에러메시지 감지
                    string tmp = driver.Url;
                    IAlert alert2 = driver.SwitchTo().Alert();
                    if (alert2 != null)
                    {
                        alert2.Accept();
                        return ResultWithError("alertstring : " + alert2.Text);
                        
                        //  driver.Close();
                    }
                    else
                    {
                        return ResultWithError("NoAlertPresentException");
                    }
                }*/

            } //end try
            catch (NoAlertPresentException)
            {
                return ResultWithError("NoAlertPresentException", false);

            }
            catch (UnhandledAlertException)
            {
                IAlert alert = driver.SwitchTo().Alert();
                if (alert != null)
                {
                    string alertString = alert.Text;
                    alert.Accept();
                    return ResultWithError("alertstring : " + alertString, false);

                    //  driver.Close();
                }
            }
            return ResultWithError("", false);
        }


        public Result shopUpload(Model_post p)
        {

            var sitem = new SiteManager();
            
            try
            {
               // this.template_string = c.template;

                if(p.shop_cate_id == null)
                {
                    return ResultWithError("홈페이지 카테고리 설정이 필요합니다", true);
                }

                WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(90));

                driver.Url = string.Format("http://{0}/xe/index.php?mid={1}&act=dispBoardWrite", sitem.getSiteURL(), p.shop_cate_id);

                /*
                if (driver.Url.IndexOf("cafe.naver.com/ArticleWrite.nhn?") < 0)
                {
                    if (driver.Url.IndexOf("iframe_url=/ArticleWrite.nhn") > 0)
                    {
                        return ResultWithError("게시물 등록 실패(글쓰기 권한 없음)", true);
                    }
                }*/

                //Console.WriteLine(driver.Url);

                //글쓰기 페이지 대기
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("iText")));

                System.Threading.Thread.Sleep(2000);

                //제목 입력
                driver.FindElement(By.Name("title")).Clear();
                driver.FindElement(By.Name("title")).SendKeys(p.title);


                //사진 업로드
                if (p.imagepaths.Count > 0)
                {
                   
                    //파일첨부
                    List<string> paths = p.imagepaths;
                    for (int i = 0; i < paths.Count(); i++)
                    {

                        paths[i] = Path.Combine(Directory.GetCurrentDirectory(), paths[i]);
                    }
                    string imagePathsString = string.Join(" \n ", paths);
                    Console.WriteLine(imagePathsString);

                    driver.FindElement(By.Id("xe-fileupload")).SendKeys(imagePathsString);
                    //wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("npu_image_list_thumb")));
                    //Threading.Thread.Sleep(2000);
                    wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("xefu-progress-status")));
                    


                    //제목, 본문 입력(사진 업로드 후 아래부분 딜레이 안주면 오류남)
                    /*
                    wait.Until<bool>((d) => { return (d.WindowHandles.Count == 1); });

                    driver.SwitchTo().Window(driver.WindowHandles.ToList().First());*/
                }


                System.Threading.Thread.Sleep(2000);
                //driver.FindElement(By.Id("elHtmlMode")).Click();
                System.Threading.Thread.Sleep(100);

                //내용 입력

                //driver.SwitchTo().Frame()
                var iframe1 = driver.FindElement(By.ClassName("cke_wysiwyg_frame"));
                driver.SwitchTo().Frame(iframe1);
                
                string header_string = p.content;
                string imgSource = driver.FindElement(By.TagName("body")).GetAttribute("innerHTML");
                Console.WriteLine(imgSource);
                string footer_string = "\n";
                string post_body = this.replaceNewLine(header_string + footer_string) + imgSource;

                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;

                var bodyElement = driver.FindElement(By.TagName("body"));

                js.ExecuteScript("var ele=arguments[0]; ele.innerHTML = arguments[1];", bodyElement, post_body);

                driver.SwitchTo().DefaultContent();

                //게시글 작성
                driver.FindElement(By.ClassName("btn_insert")).Click();
                System.Threading.Thread.Sleep(2000);


                string done_url = driver.Url;

                if (done_url.IndexOf("document_srl=") >= 0)
                {
                    string s = Regex.Split(driver.Url, "document_srl=")[1].Split('%')[0];
                    return ResultWithSuccess(s);
                }
                /*
                else
                {

                    //작성 후 에러메시지 감지
                    string tmp = driver.Url;
                    IAlert alert2 = driver.SwitchTo().Alert();
                    if (alert2 != null)
                    {
                        alert2.Accept();
                        return ResultWithError("alertstring : " + alert2.Text);
                        
                        //  driver.Close();
                    }
                    else
                    {
                        return ResultWithError("NoAlertPresentException");
                    }
                }*/

            } //end try
            catch (NoAlertPresentException)
            {
                return ResultWithError("NoAlertPresentException", false);

            }
            catch (UnhandledAlertException)
            {
                IAlert alert = driver.SwitchTo().Alert();
                if (alert != null)
                {
                    string alertString = alert.Text;
                    alert.Accept();
                    return ResultWithError("alertstring : " + alertString, false);

                    //  driver.Close();
                }
            }
            catch (Exception e)
            {
                return ResultWithError(e.Message, false);

            }
            return ResultWithError("", false);
        }
    }
}
